package com.alexander.builderdemo.enums;

/**
 *
 * @author Alex & Derek
 */
public enum MeatType {
    HAM,
    TURKEY,
    PEPPERONI,
    HAMBURGER;
}
