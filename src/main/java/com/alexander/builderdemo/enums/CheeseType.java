package com.alexander.builderdemo.enums;

/**
 *
 * @author Alex & Derek
 */
public enum CheeseType {
    SWISS,
    AMERICAN,
    PEPPER_JACK;
}
