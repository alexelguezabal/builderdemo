package com.alexander.builderdemo;

import com.alexander.builderdemo.enums.CheeseType;
import com.alexander.builderdemo.enums.MeatType;

/**
 *
 * @author Alex
 */
public class Main {

    public static void main(String[] args) {

        // Sandwhich builder
        Sandwhich sandwhich1 = new SandwhichBuilder()
                .addMeat(MeatType.HAM)
                .addCheese(CheeseType.SWISS)
                .addMeat(MeatType.PEPPERONI)
                .addBuns()
                .build();
                
        // Two different ways of making a Sandwhich Builder        
        
        SandwhichBuilder sandwhichBuilder2 = new SandwhichBuilder();
        
        // Adds the ingredients into the sandwich
        sandwhichBuilder2.addMeat(MeatType.HAMBURGER);
        sandwhichBuilder2.addCheese(CheeseType.PEPPER_JACK);
        sandwhichBuilder2.addCheese(CheeseType.AMERICAN);
        sandwhichBuilder2.addMeat(MeatType.TURKEY);
        sandwhichBuilder2.addBuns();
                
        // Prints out all the ingredients
        System.out.println(sandwhich1.toString());
        
        System.out.println("\n\n\n");
        
        System.out.println(sandwhichBuilder2.build().toString());
    }

}
