package com.alexander.builderdemo;

import com.alexander.builderdemo.enums.CheeseType;
import com.alexander.builderdemo.enums.MeatType;
import com.alexander.builderdemo.interfaces.Builder;


/**
 * @author Alex & Derek
 */
public class SandwhichBuilder implements Builder<Sandwhich> {
    
    // Instance of the sandfwhich for this builder
    private Sandwhich instance;
    
    /**
     * Default Constructor
     */
    public SandwhichBuilder() {
        this(new Sandwhich());
    }
        
    /**
     * Overload Constructor
     * @param sandwhich Sandwich to be built on
     */
    public SandwhichBuilder(Sandwhich sandwhich) {
        this.instance = sandwhich;
    }
    
    /**
     * Add cheese to the sandwich
     * @param cheeseType Type of cheese
     */
    public SandwhichBuilder addCheese(CheeseType cheeseType) {
        addAtribute(cheeseType.name() + "_CHEESE");
        return this;
    }
    
    /**
     * Add buns to the top and bottom of the sandwich
     */
    public SandwhichBuilder addBuns() {
        addAtribute("TOP_BUN", 0);
        addAtribute("BOTTOM_BUN", instance.getAttributes().size());
        return this;
    }
    
     /**
     * Add meat to the sandwich
     * @param MeatType Type of meat
     */
    public SandwhichBuilder addMeat(MeatType meatType) {
        addAtribute(meatType.name());
        return this;
    }
    
    /**
     * Builds the sandhich
     * @return the sandhich instance
     */
    public Sandwhich build() {
        return instance;
    }
    
    /**
     * Private Methods
     */
    
    /**
     * Adds an attribute to the sandwich {@code instance}
     * 
     * @param attributeName Name of the attribute to add
     */
    private void addAtribute(String attributeName) {
        instance.getAttributes().add(attributeName);
    }
    
     /**
     * Adds an attribute to the sandwich {@code instance}
     * 
     * @param attributeName Name of the attribute to add
     * @param position position of the attribute
     */
    private void addAtribute(String attributeName, int position) {
        instance.getAttributes().add(position, attributeName);
    }
}
