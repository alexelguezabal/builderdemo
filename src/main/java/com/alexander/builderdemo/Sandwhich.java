package com.alexander.builderdemo;

import java.util.ArrayList;

/**
 * @author Alex & Derek
 */
public class Sandwhich {

    /**
     * List of the attributes of this Sandwich
     */
    public ArrayList<String> attributes = new ArrayList<>();

    /**
     * @return the attributes
     */
    public ArrayList<String> getAttributes() {
        return attributes;
    }

    /**
     * @param attributes the attributes to set
     */
    public void setAttributes(ArrayList<String> attributes) {
        this.attributes = attributes;
    }
    
    /**
     * ToString method
     * @return All of the attributes printed.
     */
    @Override
    public String toString() {
       StringBuilder sb = new StringBuilder("Ingredients for this Sandwich.\n");
       attributes.forEach(n -> sb.append("\t"+ n + "\n"));
       
       return sb.toString();
    }
    
}
