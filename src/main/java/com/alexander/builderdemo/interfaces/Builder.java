/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.alexander.builderdemo.interfaces;

/**
 * @author Alex
 * @param Type of object that this builder is building
 */
public interface Builder<T> {
    
    // All builder methods here
    
    /**
     * Gets a copy of this builder
     * 
     * @return A instance of class T.
     */
    public T build();
    
}
